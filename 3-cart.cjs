const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/ 

//console.log(products[0]);

const returnedData=products.map((element) =>
{
    const keys=Object.keys(element);

        correctedData=keys.reduce((accumulator, item) =>
        {
            const cartItemInfo=element[item];
            //console.log(cartItemInfo);
            if(Array.isArray(cartItemInfo))
            {
               const innerReducedData=cartItemInfo.reduce((innerAcc,innerObj) =>
                {
                    innerAcc={...innerAcc,...innerObj};
                    //console.log(innerAcc);
                    return innerAcc;  
                },{});
                //console.log(innerReducedData);
                    //Add Outer Key as a new key pair 
                    const innerReducedDataWithOuterKey=Object.entries(innerReducedData).reduce((acc, [key, value]) => {
                        acc[key] = { ...value, productCategory: item };
                        return acc;
                    }, {});

                //console.log(innerReducedDataWithOuterKey);
                accumulator={...accumulator,...innerReducedDataWithOuterKey}
                return accumulator;
            
            }else{
                accumulator[item]=element[item];
            }

            return accumulator;
        },{});

        return correctedData;
});



//Solution 1

const priceOver65=returnedData.map((element) =>
{
    const keys=Object.entries(element);

    const filteredData=keys.filter((itemInfo) =>
    {
       return itemInfo[1].price.slice(1) >65;
    });

    return Object.fromEntries(filteredData);
});

console.log(priceOver65);

console.log("--------------------------------------------")
// Solution 2

const quantityOver1=returnedData.map((element) =>
{
    const keys=Object.entries(element);

    const filteredData=keys.filter((itemInfo) =>
    {
       return itemInfo[1].quantity > 1;
    });

    return Object.fromEntries(filteredData);
});

console.log(quantityOver1);

console.log("--------------------------------------------")
// Solution 3

const fragileItems=returnedData.map((element) =>
{
    const keys=Object.entries(element);

    const filteredData=keys.filter((itemInfo) =>
    {
       return itemInfo[1].type === 'fragile';
    });

    return Object.fromEntries(filteredData);
});

console.log(fragileItems);

console.log("--------------------------------------------")
// Solution 4

const mostAndLeastExpensive=returnedData.map((element) =>
{
    const keys=Object.entries(element);

    const sortedData =keys.sort((item1,item2) =>
    {
        item1MRP= Number(item1[1].price.slice(1))/item1[1].quantity;
        item2MRP= Number(item2[1].price.slice(1))/item2[1].quantity;

        return item1MRP-item2MRP;
    })
    //console.log(sortedData);
    return sortedData;
});

console.log([mostAndLeastExpensive[0][0],mostAndLeastExpensive[0][mostAndLeastExpensive[0].length -1]]);

console.log("--------------------------------------------")
// Solution 5

const groupByStateOfMatter=returnedData.map((element) =>
{
    const keys=Object.entries(element);

    const sortKey=keys.map((key) =>
    {
        if (key[0] == 'shampoo' || key[0] === "Hair-oil") {
            return ['liquid', key]
        } else {
            return ['solid', key]
        }
    })
    .reduce((output, item) => 
    {
       // console.log(item[0]);
        output[item[0]].push(item[1])
        return output
    }, {
        solid: [],
        liquid: [],
        gas: []
    })
    //console.log(sortKey)
    return sortKey;

});


console.log(groupByStateOfMatter)

